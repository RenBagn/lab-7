public class Board{
	private Square[][] tictactoeBoard;
	
	public Board(){
		tictactoeBoard= new Square[][]{{Square.BLANK,Square.BLANK,Square.BLANK},
		{Square.BLANK,Square.BLANK,Square.BLANK},{Square.BLANK,Square.BLANK,Square.BLANK}};
	}
	
	public String toString(){
		String r= "  0 1 2";
		r+="\n";
		for(int i=0; i<tictactoeBoard.length; i++){
			r+=i + " ";
			for(int o=0;o<tictactoeBoard[i].length; o++){
				
				r+=tictactoeBoard[i][o]+" ";
			}
			r+= "\n";
		}
		
		return r;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if(col>2||row>2){
			return false;
		}
		else if(tictactoeBoard[row][col]==Square.BLANK){
			tictactoeBoard[row][col]=playerToken;
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean checkIfFull(){
		for(int i=0;i<tictactoeBoard.length;i++){
			for(int j=0; j<tictactoeBoard[i].length;j++){
				if(tictactoeBoard[i][j]==Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(int i=0; i<tictactoeBoard.length;i++){
				if((tictactoeBoard[i][0]==playerToken)&&(tictactoeBoard[i][1]==playerToken)&&(tictactoeBoard[i][2]==playerToken)){
					return true;
				}
			}
			return false;
	}
	private boolean checkIfWinningVertical(Square playerToken){
			for(int i=0; i<tictactoeBoard.length;i++){
				if((tictactoeBoard[0][i]==playerToken)&&(tictactoeBoard[1][i]==playerToken)&&(tictactoeBoard[2][i]==playerToken)){
					return true;
				}
			}
			return false;
	}
	public boolean checkIfWinning(Square playerToken){
		boolean checkVertical=false;
		boolean checkHorizontal=false;
		checkVertical=checkIfWinningVertical(playerToken);
		checkHorizontal=checkIfWinningHorizontal(playerToken);
		if(checkVertical==true || checkHorizontal==true){
			return true;
		}
		else{
			return false;
		}
	}
}
	


