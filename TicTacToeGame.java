import java.util.Scanner;
public class TicTacToeGame{
	public static void main(String []args){
		Scanner reader= new Scanner(System.in);
		System.out.println("Welcome to TicTacToe!");
		System.out.println("Player 1's token: X");
		System.out.println("Player 2's token: O");
		Board tictacToe = new Board();
		boolean gameOver= false;
		int player=1;
		Square playerToken= Square.X;
		while(gameOver==false){
			System.out.println("Player "+player+ ": It's Your Turn!");
			System.out.println(tictacToe);
			if(player==1){
				playerToken=Square.X;
			}
			else{
				playerToken=Square.O;
			}
			int row;
			int col;
			boolean check=false;
			while(check==false){
				row= reader.nextInt();
			    col= reader.nextInt();
				check=tictacToe.placeToken(row, col, playerToken);
			}
			check=tictacToe.checkIfFull();
			if(check==true){
				gameOver=true;
				System.out.println("It's a tie!");
				System.out.println(tictacToe);
			}
			check=tictacToe.checkIfWinning(playerToken);
			if(check==true){
				System.out.println("Player " +player+ " is the winner!");
				gameOver=true;
				System.out.println(tictacToe);
			}
			
			else{
				player++;
				if(player>2){
					player=1;
				}
			}
		}
	}
}
				
			
		